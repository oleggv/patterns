# README #

This is examples of Design patterns, implemented on Java. [list](https://ru.wikipedia.org/wiki/%D0%A8%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)

### Design patterns ###

### Creational

* Abstract Factory ([Desc](https://ru.wikipedia.org/wiki/%D0%90%D0%B1%D1%81%D1%82%D1%80%D0%B0%D0%BA%D1%82%D0%BD%D0%B0%D1%8F_%D1%84%D0%B0%D0%B1%D1%80%D0%B8%D0%BA%D0%B0_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)), [Code](https://bitbucket.org/slickstars/patterns/src/a581be46f73d7f5290ab0b2574832ac6cef7644f/AbstractFactory/src/com/?at=master), [UML](https://bitbucket.org/slickstars/patterns/src/6c6f638c9324be0a71d816f4e7ebc6529052bb52/AbstractFactory/abstract-factory.bmp?at=master&fileviewer=file-view-default))
* Builder ([Desc](https://ru.wikipedia.org/wiki/%D0%A1%D1%82%D1%80%D0%BE%D0%B8%D1%82%D0%B5%D0%BB%D1%8C_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)), [Code](https://bitbucket.org/slickstars/patterns/src/a581be46f73d7f5290ab0b2574832ac6cef7644f/Builder/src/com/?at=master), [UML](https://bitbucket.org/slickstars/patterns/src/6c6f638c9324be0a71d816f4e7ebc6529052bb52/Builder/builder.bmp?at=master&fileviewer=file-view-default))
* Factory Method ([Desc](https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B1%D1%80%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BC%D0%B5%D1%82%D0%BE%D0%B4_(%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F)), [Code](https://bitbucket.org/slickstars/patterns/src/a581be46f73d7f5290ab0b2574832ac6cef7644f/FactoryMethod/src/com/?at=master), [UML](https://bitbucket.org/slickstars/patterns/src/6c6f638c9324be0a71d816f4e7ebc6529052bb52/FactoryMethod/factory-method.bmp?at=master&fileviewer=file-view-default))

### Structural

* Adapter ([Desc](https://ru.wikipedia.org/wiki/%D0%90%D0%B4%D0%B0%D0%BF%D1%82%D0%B5%D1%80_%28%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F%29), [Code](https://bitbucket.org/slickstars/patterns/src/ca373829c0d967e8ef7e9cf7a7b0210f2bbde7a4/Adapter/src/com/?at=master), [UML](https://bitbucket.org/slickstars/patterns/src/ca373829c0d967e8ef7e9cf7a7b0210f2bbde7a4/Adapter/adapter.bmp?at=master&fileviewer=file-view-default))
* Bridge ([Desc](https://ru.wikipedia.org/wiki/%D0%9C%D0%BE%D1%81%D1%82_%28%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F%29), [Code](https://bitbucket.org/slickstars/patterns/src/7bf37c91770bdff568f4bc7af38b1aca72dab8c0/Bridge/src/com/?at=master), [UML](https://bitbucket.org/slickstars/patterns/src/7bf37c91770bdff568f4bc7af38b1aca72dab8c0/Bridge/bridge.bmp?at=master&fileviewer=file-view-default))
* Facade ([Desc](https://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D1%81%D0%B0%D0%B4_%28%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F%29), [Code](https://bitbucket.org/slickstars/patterns/src/db96895164aefd635dd04c16b2c000e9b3a51eea/Facade/src/com/?at=master), [UML](https://bitbucket.org/slickstars/patterns/src/db96895164aefd635dd04c16b2c000e9b3a51eea/Facade/facade.bmp?at=master&fileviewer=file-view-default))

### Behavioral

* Command ([Desc](https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D0%B0_%28%D1%88%D0%B0%D0%B1%D0%BB%D0%BE%D0%BD_%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D1%8F%29), [Code](https://bitbucket.org/slickstars/patterns/src/a24b8bec7b174e7f4fd6a49ba89dfe1faa382816/Command/src/com/?at=master), [UML](https://bitbucket.org/slickstars/patterns/src/a24b8bec7b174e7f4fd6a49ba89dfe1faa382816/Command/command.bmp?at=master&fileviewer=file-view-default))