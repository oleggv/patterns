package com;

import java.util.ArrayList;

import com.colors.Green;
import com.colors.Red;
import com.shapes.Rectangle;
import com.shapes.Triangle;

public class Main {

	public static void main(String[] args) {
		ArrayList<Shape> shapes = new ArrayList<>();
		shapes.add( new Triangle(new Green()) );
		shapes.add( new Triangle(new Red()) );
		shapes.add( new Rectangle(new Green()) );
		
		for(Shape shape : shapes) {
			shape.applyColor();
		}
	}
}
