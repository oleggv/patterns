package com.shapes;

import com.Color;
import com.Shape;

public class Rectangle extends Shape {

	public Rectangle(Color color) {
		super(color);
	}

	@Override
	public void applyColor() {
		System.out.print("Rectangle is applied color: ");
		color.applyColor();
	}

}
