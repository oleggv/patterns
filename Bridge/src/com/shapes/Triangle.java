package com.shapes;

import com.Color;
import com.Shape;

public class Triangle extends Shape {

	public Triangle(Color color) {
		super(color);
	}

	@Override
	public void applyColor() {
		System.out.print("Triangle is applied color: ");
		color.applyColor();
	}
	
}
