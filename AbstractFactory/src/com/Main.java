package com;

import java.util.ArrayList;

import com.factories.AppleFactory;
import com.factories.CompanyFactory;
import com.factories.SamsungFactory;
import com.products.abstracts.Laptop;
import com.products.abstracts.Phone;

public class Main {

	public static void main(String[] args) {
		ArrayList<CompanyFactory> factories = new ArrayList<>();
		
		factories.add(new AppleFactory());
		factories.add(new SamsungFactory());
		
		for(CompanyFactory factory : factories) {
			System.out.println("Factory name: " + factory.getClass().getName());
			
			Phone phone = factory.createPhone();
			System.out.println("Created phone Name: " + phone.getName() + " Model: " + phone.getModel() + " Description: " + phone.getDescription());
			
			Laptop laptop = factory.createLaptop();
			System.out.println("Created laptop Name: " + laptop.getName() + " Model: " + laptop.getModel() + " Keyboard: " + laptop.getKeyboard() + " Description: " + laptop.getDescription() + "\n");
		}
	}
}
