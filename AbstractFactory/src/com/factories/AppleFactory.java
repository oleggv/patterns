package com.factories;

import com.products.Mac;
import com.products.iPhone;
import com.products.abstracts.Laptop;
import com.products.abstracts.Phone;

public class AppleFactory extends CompanyFactory {

	@Override
	public Phone createPhone() {
		return new iPhone();
	}

	@Override
	public Laptop createLaptop() {
		return new Mac();
	}
	
}
