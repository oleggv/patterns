package com.factories;

import com.products.abstracts.Laptop;
import com.products.abstracts.Phone;

public abstract class CompanyFactory {
	public abstract Phone createPhone();
	public abstract Laptop createLaptop();
}
