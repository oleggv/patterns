package com.factories;

import com.products.SamsungLaptop;
import com.products.SamsungS4;
import com.products.abstracts.Laptop;
import com.products.abstracts.Phone;

public class SamsungFactory extends CompanyFactory {

	@Override
	public Phone createPhone() {
		return new SamsungS4();
	}

	@Override
	public Laptop createLaptop() {
		return new SamsungLaptop();
	}

}
