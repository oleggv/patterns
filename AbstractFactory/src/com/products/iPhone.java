package com.products;

import com.products.abstracts.Phone;

public class iPhone extends Phone {

	public iPhone() {
		setName("iPhone");
		setModel("6");
	}
	
	@Override
	public String getDescription() {
		return "Iphone 6";
	}

}
