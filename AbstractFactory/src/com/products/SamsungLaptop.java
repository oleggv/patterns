package com.products;

import com.products.abstracts.Laptop;

public class SamsungLaptop extends Laptop {

	public SamsungLaptop() {
		setName("Samsung");
		setModel("XSS-35");
		setKeyboard("Standart");
	}
	
	@Override
	public String getDescription() {
		return "Samsung laptop on Windows 10";
	}

}
