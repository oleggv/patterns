package com.products;

import com.products.abstracts.Laptop;

public class Mac extends Laptop {

	public Mac() {
		setName("Macbook");
		setModel("Pro");
		setKeyboard("Mini");
	}
	
	@Override
	public String getDescription() {
		return "Macbook pro";
	}

}
