package com.products;

import com.products.abstracts.Phone;

public class SamsungS4 extends Phone {
	
	public SamsungS4() {
		setName("Samsung");
		setModel("s4");
	}
	
	@Override
	public String getDescription() {
		return "Samsung s4 on Android";
	}

}
