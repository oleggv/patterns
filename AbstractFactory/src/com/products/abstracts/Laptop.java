package com.products.abstracts;

public abstract class Laptop {
	private String name;
	private String model;
	private String keyboard;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public String getKeyboard() {
		return keyboard;
	}
	
	public void setKeyboard(String keyboard) {
		this.keyboard = keyboard;
	}
	
	public abstract String getDescription();
}
