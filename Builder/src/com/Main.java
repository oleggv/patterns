package com;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		Director director = new Director();
		
		ArrayList<PhoneBuilder> builders = new ArrayList<>();
		
		builders.add(new ApplePhhoneBuilder());
		builders.add(new GooglePhoneBuilder());
		
		for(PhoneBuilder builder : builders) {
			director.setPhoneBuilder(builder);
			director.constructPhone();
			
			Phone phone = director.getPhone();
			
			System.out.println("Phone builded:\n" + "Name: " + phone.getName() + " Model: " + phone.getModel() + " OS: " + phone.getOs());
		}
		
		director.setPhoneBuilder(new ApplePhhoneBuilder());
		director.constructPhone();
		
		
	}

}
