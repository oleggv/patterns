package com;

public class Director {

	private PhoneBuilder builder;
	
	public void setPhoneBuilder(PhoneBuilder builder) {
		this.builder = builder;
	}
	
	public Phone getPhone() {
		return builder.getPhone();
	}
	
	public void constructPhone() {
		builder.createNewPhone();
		builder.buildParams();
	}
}
