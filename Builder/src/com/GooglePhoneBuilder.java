package com;

public class GooglePhoneBuilder extends PhoneBuilder {

	@Override
	public void buildParams() {
		phone.setName("Nexus");
		phone.setModel("9");
		phone.setOs("Android");
	}

}
