package com;

/*
 * Concrete builder
 * */
public class ApplePhhoneBuilder extends PhoneBuilder {

	@Override
	public void buildParams() {
		phone.setName("iPhone");
		phone.setModel("6");
		phone.setOs("iOS");
	}

}
