package com;

/*
 * Abstract builder
 * */
public abstract class PhoneBuilder {
	protected Phone phone;
	
	public Phone getPhone() {
		return phone;
	}
	
	public void createNewPhone() {
		this.phone = new Phone();
	}
	
	public abstract void buildParams();
}
