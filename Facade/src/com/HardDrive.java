package com;

import java.util.Random;

public class HardDrive {
	public void init() {
		System.out.println("HardDrive: initializing..");
	}
	
	public int read(int pos) {
		
		return new Random().nextInt(pos * 5);
	}
	
	public boolean write(byte data) {
		if(data < 0) return false;
		
		return true;
	}
}
