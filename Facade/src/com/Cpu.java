package com;

public class Cpu {
	
	public void jump(long position) {
		System.out.println("Cpu: jump to " + position);
	}
	
	public void execute() {
		System.out.println("Cpu: executing..");
	}
}
