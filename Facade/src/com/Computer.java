package com;

public class Computer {
	
	private Cpu cpu;
	private HardDrive hardDrive;
	private Memory memory;
	private Video video;
	
	public Computer() {
		this.cpu = new Cpu();
		this.hardDrive = new HardDrive();
		this.memory = new Memory();
		this.video = new Video();
	}
	
	public void start() {
		memory.load(0x10, hardDrive.read(0x15));
		cpu.jump(0x10);
		video.initVideoDriver("Intel");
		cpu.execute();
	}
}
