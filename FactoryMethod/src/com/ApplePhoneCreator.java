package com;

import com.products.IPhone;
import com.products.Phone;

public class ApplePhoneCreator extends PhoneCreator {

	@Override
	public Phone factoryMethod() {
		IPhone iphone = new IPhone();
		iphone.setName("iPhone 6");
		iphone.setOs("iOS");
		return iphone;
	}

}
