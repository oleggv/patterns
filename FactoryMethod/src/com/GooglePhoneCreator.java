package com;

import com.products.Nexus;
import com.products.Phone;

public class GooglePhoneCreator extends PhoneCreator {

	@Override
	public Phone factoryMethod() {
		Nexus nexus = new Nexus();
		nexus.setName("Nexus 9");
		nexus.setOs("Android");
		return nexus;
	}

}
