package com;

import java.util.ArrayList;

import com.products.IPhone;
import com.products.Nexus;
import com.products.Phone;

public class Main {

	public static void main(String[] args) {
		ArrayList<PhoneCreator> creators = new ArrayList<>();
		
		creators.add(new ApplePhoneCreator());
		creators.add(new GooglePhoneCreator());
		
		for(PhoneCreator creator : creators) {
			
			Phone phone = creator.factoryMethod(); // create phone with concrete phone factory
			
			System.out.print("Phone created:\n" + "Name: " + phone.getName() + " OS: " + phone.getOs());
			
			if(phone instanceof IPhone) {
				IPhone iPhone = (IPhone) phone;
				System.out.println(" Memory: " + iPhone.getMemory() + "\n");
			}
			else if(phone instanceof Nexus) {
				Nexus nexus = (Nexus) phone;
				System.out.println(" SDCard: " + nexus.getCard() + "\n");
			}
		}
	}

}
