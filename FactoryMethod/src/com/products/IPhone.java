package com.products;

public class IPhone extends Phone {
	
	private String memory;

	public IPhone() {
		setMemory("16gb");
	}
	
	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}
}
