package com.products;

public class Nexus extends Phone {
	
	public Nexus() {
		setCard("32gb");
	}
	
	private String sdCard;

	public String getCard() {
		return sdCard;
	}

	public void setCard(String sdCard) {
		this.sdCard = sdCard;
	}
}
