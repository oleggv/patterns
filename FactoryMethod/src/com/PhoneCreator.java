package com;

import com.products.Phone;

public abstract class PhoneCreator {
	public abstract Phone factoryMethod();
}
