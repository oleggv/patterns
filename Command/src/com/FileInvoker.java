package com;

import com.command.Command;

public class FileInvoker implements Command {

	private Command command;
	
	public FileInvoker(Command command) {
		this.command = command;
	}
	
	@Override
	public void execute() {
		command.execute();
	}
}
