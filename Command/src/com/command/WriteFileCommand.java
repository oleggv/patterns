package com.command;

import com.filesystem.FileSystemReceiver;

public class WriteFileCommand implements Command {
	
	private FileSystemReceiver fileSystem;
	
	public WriteFileCommand(FileSystemReceiver fileSystem) {
		this.fileSystem = fileSystem;
	}
	
	@Override
	public void execute() {
		fileSystem.writeFile();
	}

}
