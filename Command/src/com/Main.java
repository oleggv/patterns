package com;

import java.io.Closeable;
import java.util.ArrayList;

import com.command.CloseFileCommand;
import com.command.OpenFileCommand;
import com.command.WriteFileCommand;
import com.filesystem.FileSystemReceiver;
import com.filesystem.UnixFileSystemReceiver;
import com.filesystem.WindowsFileSystemReceiver;

public class Main {

	public static void main(String[] args) {
		
		FileInvoker invoker;
		ArrayList<FileSystemReceiver> fileSystems = new ArrayList<>();
		
		fileSystems.add(new UnixFileSystemReceiver());
		fileSystems.add(new WindowsFileSystemReceiver());
		
		for(FileSystemReceiver fs : fileSystems) {
			
			OpenFileCommand openFileCommand = new OpenFileCommand(fs);
			invoker = new FileInvoker(openFileCommand);
			invoker.execute();

			WriteFileCommand writeFileCommand = new WriteFileCommand(fs);
			invoker = new FileInvoker(writeFileCommand);
			invoker.execute();
			
			CloseFileCommand closeFileCommand = new CloseFileCommand(fs);
			invoker = new FileInvoker(closeFileCommand);
			invoker.execute();
		}
	}
}
