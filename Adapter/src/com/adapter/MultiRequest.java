package com.adapter;

import com.Request;

public class MultiRequest implements Request {

	@Override
	public void send(String message) {
		System.out.println("\nMulti request:");
		DoubleRequest doubleRequest = new DoubleRequest();
		doubleRequest.send(message);
	}
}
