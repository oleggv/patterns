package com.adapter;

import com.HttpRequest;
import com.TcpRequest;

public class DoubleRequest {
	
	public void send(String message) {
		HttpRequest httpRequest = new HttpRequest();
		TcpRequest tcpRequest = new TcpRequest();
		
		httpRequest.send(message.toUpperCase());
		tcpRequest.send(message.toUpperCase());
		
	}
}
