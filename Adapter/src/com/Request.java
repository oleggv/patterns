package com;

public interface Request {
	
	void send(String message);
}
