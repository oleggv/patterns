package com;

import java.util.ArrayList;

import com.adapter.MultiRequest;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Request> requests = new ArrayList<>();
		
		requests.add(new HttpRequest());
		requests.add(new TcpRequest());
		requests.add(new MultiRequest());
		
		for(Request request : requests) {
			request.send("Hello");
		}
	}

}
